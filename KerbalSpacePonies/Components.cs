using System;
using UnityEngine;

namespace KerbalSpacePonies
{
    public class ShipInteriorModule: MonoBehaviour
    {
        public void Start()
        {
            // Ponifies all kerbals inside the ship's internal space after it was loaded.
            // See comments for TRIvaModelModule in TextureReplacer's code
            // in TRIvaModelModule.cs and Personaliser.cs files for more details
            foreach (Kerbal kerbal in GetComponentsInChildren<Kerbal>()) {
                if (kerbal.GetComponent<IvaModule>() == null) {
                    kerbal.gameObject.AddComponent<IvaModule>();
                }
            }

            Destroy(this);
        }
    }

    public class IvaModule: MonoBehaviour
    {
        public void Start()
        {
            var kerbal = GetComponent<Kerbal>();
            new Ponifier(kerbal, kerbal.protoCrewMember).Ponify();
            gameObject.AddComponent<VisibilityChecker>();
            Destroy(this);
        }
    }
    
    public class EvaModule: PartModule
    {
        private bool isInitialised = false;
        
        public override void OnStart(StartState state)
        {
            if (!isInitialised)
            {
                isInitialised = true;
                new Ponifier(part, part.protoModuleCrew[0]).Ponify();
            }
        }
    }

    public class VisibilityChecker: MonoBehaviour
    {
        private Renderer head, eyes, mane, horn, kerbalHead;

        public void Start()
        {
            foreach (var smr in GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                switch (smr.name)
                {
                    case "headMesh01":
                    case "mesh_female_kerbalAstronaut01_kerbalGirl_mesh_polySurface51":
                    case "headMesh":
                        kerbalHead = smr;
                        break;
                    case "ponyHead":
                        head = smr;
                        break;
                    case "ponyEyes":
                        eyes = smr;
                        break;
                    case "mane":
                        mane = smr;
                        break;
                    case "horn":
                        horn = smr;
                        break;
                }
            }
        }

        public void Update()
        {
            if (!head) return;

            // Hide all head meshes when in IVA first-person view
            bool visible = kerbalHead.enabled;
            if (head) head.enabled = visible;
            if (eyes) eyes.enabled = visible;
            if (mane) mane.enabled = visible;
            if (horn) horn.enabled = visible;
        }
    }
}
