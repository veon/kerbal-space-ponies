using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace KerbalSpacePonies
{
    public class PonyPart
    {
        public Mesh mesh;
        public Material material;

        public PonyPart(Mesh mesh, Material material)
        {
            this.mesh = mesh;
            this.material = material;
        }
    }

    public class Pony
    {
        public string name;
        public PonyPart head, mane, eyes, horn;
        public string trait = "Pilot";
        public float courage = 0.5f, stupidity = 0.5f, scale = 1.0f;
        public ProtoCrewMember.Gender gender = ProtoCrewMember.Gender.Female;
    }

    public class PonyConfigs
    {
        public static PonyConfigs instance = null;
        private Dictionary<string, Pony> ponies = new Dictionary<string, Pony>();
        private Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();
        private Matrix4x4 bindpose;

        public IEnumerable<string> PonyNames { get { return ponies.Keys; } }

        public PonyConfigs()
        {
        }

        private Pony CreatePony(string name)
        {
            var pony = new Pony();
            pony.name = name;
            return ponies[name] = pony;
        }

        private static string DIR = "KerbalSpacePonies/";
        private static string TEX_DIR = DIR + "Textures/";
        private static string MODELS_DIR = "GameData/" + DIR + "Models/";

        public void Load()
        {
            Pony pony;
            FillTexturesDict();

            bindpose = GetBindPose();
            var ponyHead = LoadMesh("Head");
            var eyes = LoadMesh("Eyes");
            var ponyHorn = LoadMesh("Horn");
            var rainbowMane = LoadMesh("RainbowMane");
            var sweetMane = LoadMesh("BonbonMane");
            var trixieMane = LoadMesh("TrixieMane");
            var twilightMane = LoadMesh("TwilightMane");
            var lyraMane = LoadMesh("LyraMane");
            var applejackMane = LoadMesh("ApplejackMane");
            var starlightMane = LoadMesh("StarlightMane");
            var berryMane = LoadMesh("BerryMane");
            var spitfireMane = LoadMesh("SpitfireMane");
            var octaviaMane = LoadMesh("OctaviaMane");

            var blueEyes = "blue_eyes.png";
            var twilightEyes = "twilight_eyes.png";
            var pinkieEyes = "pinkie_eyes.png";
            var applejackEyes = "applejack_eyes.png";
            var trixieEyes = "trixie_eyes.png";
            var appleEyes = "apple_eyes.png";
            var sunsetEyes = "sunset_eyes.png";
            var junebugEyes = "junebug_eyes.png";
            
            var bodyTexture = LoadTexture(TEX_DIR + "body.png");
            var hornTexture = LoadTexture(TEX_DIR + "horn.png");

            float foalScale = 0.75f;

            {
                pony = CreatePony("Twilight Sparkle");
                var twilightCoatColor = new Color(199/255f, 157/255f, 215/255f);
                var twilightManeColor = new Color (54/255f, 59/255f, 116/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, twilightCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + twilightEyes));
                pony.mane = new PonyPart(twilightMane, CreateMaterial(TEX_DIR + "twilight_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, twilightCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.613453f;
                pony.stupidity = 0.0f;
            }

            {
                pony = CreatePony("Rainbow Dash");
                var dashCoatColor = new Color(135/255f, 209/255f, 242/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, dashCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "rainbow_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(TEX_DIR + "rainbow_mane.png"));
                pony.trait = "Pilot";
                pony.courage = 1.0f;
                pony.stupidity = 0.6f;
            }

            {
                pony = CreatePony("Pinkie Pie");
                var pinkieCoatColor = new Color(248/255f, 185/255f, 206/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, pinkieCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + pinkieEyes));
                pony.mane = new PonyPart(LoadMesh("PinkieMane"), CreateMaterial(TEX_DIR + "pinkie_mane.png"));
                pony.trait = "Engineer";
                pony.courage = 0.83435f;
                pony.stupidity = 0.1276585f;
            }

            {
                pony = CreatePony("Applejack");
                pony.head = new PonyPart(ponyHead, CreateMaterial(TEX_DIR + "applejack_body.png"));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + applejackEyes));
                pony.mane = new PonyPart(applejackMane, CreateMaterial(TEX_DIR + "applejack_mane.png"));
                pony.trait = "Engineer";
                pony.courage = 0.8859576f;
                pony.stupidity = 0.0996755f;
            }

            {
                pony = CreatePony("Fluttershy");
                var flutterCoatColor = new Color(253 / 255f, 246 / 255f, 175 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, flutterCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "fluttershy_eyes.png"));
                pony.mane = new PonyPart(trixieMane, CreateMaterial(new Color(243 / 255f, 185 / 255f, 216 / 255f)));
                pony.trait = "Scientist";
                pony.courage = 0f;
                pony.stupidity = 0.1624005f;
            }

            {
                pony = CreatePony("Rarity");
                var rareCoatColor = new Color(240 / 255f, 242 / 255f, 243 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, rareCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "rarity_eyes.png"));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(new Color(94 / 255f, 81 / 255f, 163 / 255f)));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, rareCoatColor));
                pony.trait = "Pilot";
                pony.courage = 0.13945538f;
                pony.stupidity = 0.04057053f;
            }
            
            {
                pony = CreatePony("Apple Bloom");
                var applebloomCoatColor = new Color(232 / 255f, 235 / 255f, 143 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, applebloomCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + appleEyes));
                //I think they're meant to be Apple Bumpkin's eyes, but as far as I can tell, they're a perfect match to AB's.
                pony.mane = new PonyPart(applejackMane, CreateMaterial(new Color(248 / 255f, 65 / 255f, 95 / 255f)));
                pony.trait = "Engineer";
                pony.courage = 0.8259576f;
                pony.stupidity = 0.4496755f;
                pony.scale = foalScale;
            }
            
            {
                pony = CreatePony("Sweetie Belle");
                var sweetiebelleCoatColor = new Color(237 / 255f, 237 / 255f, 237 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, sweetiebelleCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + junebugEyes));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(TEX_DIR + "sweetiebelle_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, sweetiebelleCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.0259576f;
                pony.stupidity = 0.1236755f;
                pony.scale = foalScale;
            }
            
            {
                pony = CreatePony("Scootaloo");
                var scootalooCoatColor = new Color(254 / 255f, 185 / 255f, 98 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, scootalooCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + sunsetEyes));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(new Color(192 / 255f, 91 / 255f, 146 / 255f)));
                pony.trait = "Pilot";
                pony.courage = 0.8000576f;
                pony.stupidity = 0.6236755f;
                pony.scale = foalScale;
            }
            
            {
                pony = CreatePony("Lyra Heartstrings");
                var lyraCoatColor = new Color(148/255f, 1f, 220/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, lyraCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "lyra_eyes.png"));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "lyra_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, lyraCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.6111993f;
                pony.stupidity = 0.4804350f;
            }
            
            {
                pony = CreatePony("Bon Bon");
                var bonbonCoatColor = new Color(245/255f, 247/255f, 217/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, bonbonCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "bonbon_eyes.png"));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(TEX_DIR + "bonbon_mane.png"));
                pony.trait = "Scientist";
                pony.courage = 0.6295045f;
                pony.stupidity = 0.1628060f;
            }

            {
                pony = CreatePony("Trixie Lulamoon");
                var trixieCoatColor = new Color(85/255f, 172/255f, 243/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, trixieCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + trixieEyes));
                pony.mane = new PonyPart(trixieMane, CreateMaterial(TEX_DIR + "trixie_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, trixieCoatColor));
                pony.trait = "Engineer";
                pony.courage = 0.9832337f;
                pony.stupidity = 0.6583056f;
            }

            {
                pony = CreatePony("Derpy Hooves");
                var derpyCoatColor = new Color(193/255f, 197/255f, 211/255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, derpyCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "derpy_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(239/255f, 244/255f, 180/255f)));
                pony.trait = "Pilot";
                pony.courage = 0.728016f;
                pony.stupidity = 0.7630379f;
            }

            {
                pony = CreatePony("Moondancer");
                var moonCoatColor = new Color(249 / 255f, 250 / 255f, 213 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, moonCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + twilightEyes));
                pony.mane = new PonyPart(twilightMane, CreateMaterial(TEX_DIR + "moon_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, moonCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.345f;
                pony.stupidity = 0.046f;
            }

            {
                pony = CreatePony("Maud Pie");
                var maudCoatColor = new Color(193 / 255f, 193 / 255f, 201 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, maudCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "maud_eyes.png"));
                pony.mane = new PonyPart(twilightMane, CreateMaterial(TEX_DIR + "maud_mane.png"));
                pony.trait = "Scientist";
                pony.courage = 0.996f;
                pony.stupidity = 0.112f;
            }

            {
                pony = CreatePony("Octavia");
                var coatColor = new Color(0xBA / 255f, 0xB8 / 255f, 0xAF / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, coatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + twilightEyes));
                pony.mane = new PonyPart(octaviaMane, CreateMaterial(TEX_DIR + "octavia_mane.png"));
                pony.trait = "Scientist";
                pony.courage = 0.4174f;
                pony.stupidity = 0.0541f;
            }

            {
                pony = CreatePony("Vinyl Scratch");
                var coatColor = new Color(0xFE / 255f, 0xFD / 255f, 0xE7 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, coatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + twilightEyes));
                pony.mane = new PonyPart(LoadMesh("VinylMane"), CreateMaterial(TEX_DIR + "vinyl_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, coatColor));
                pony.trait = "Engineer";
                pony.courage = 0.8490f;
                pony.stupidity = 0.3679f;
            }

            /* The Maretians */

            {
                pony = CreatePony("Starlight Glimmer");
                var coatColor = new Color(0xED / 255f, 0xBB / 255f, 0xF3 / 255f);
                var maneColor = new Color(0x62 / 255f, 0x24 / 255f, 0x89 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, coatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + sunsetEyes));
                pony.mane = new PonyPart(starlightMane, CreateMaterial(TEX_DIR + "starlight_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, coatColor));
                pony.trait = "Scientist";
                pony.courage = 0.5387f;
                pony.stupidity = 0.0391f;
            }

            {
                pony = CreatePony("Cherry Berry");
                var coatColor = new Color(0xFF / 255f, 0x80 / 255f, 0xBF / 255f);
                var maneColor = new Color(0xFC / 255f, 0xF2 / 255f, 0x60 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, coatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + sunsetEyes));
                pony.mane = new PonyPart(berryMane, CreateMaterial(maneColor));
                pony.trait = "Pilot";
                pony.courage = 0.956f;
                pony.stupidity = 0.0373f;
            }

            /* Other background ponies */

            {
                pony = CreatePony("Helia");
                pony.head = new PonyPart(ponyHead, CreateMaterial(TEX_DIR + "helia_body.png"));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "helia_eyes.png"));
                pony.mane = new PonyPart(twilightMane, CreateMaterial(TEX_DIR + "helia_mane.png"));
                pony.trait = "Engineer";
                pony.courage = 0.706f;
                pony.stupidity = 0.331f;
            }

            {
                pony = CreatePony("Sunshower Raindrops");
                var raindropsCoatColor = new Color(248 / 255f, 231 / 255f, 133 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, raindropsCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "raindrops_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(136 / 255f, 234 / 255f, 218 / 255f)));
                pony.trait = "Pilot";
                pony.courage = 0.238f;
                pony.stupidity = 0.37f;
            }

            {
                pony = CreatePony("Apple Bumpkin");
                var bumpkinCoatColor = new Color(251 / 255f, 249 / 255f, 177 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, bumpkinCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + appleEyes));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(TEX_DIR + "apple_mane.png"));
                pony.trait = "Engineer";
                pony.courage = 0.613f;
                pony.stupidity = 0.552f;
            }

            {
                pony = CreatePony("Spitfire");
                var spitCoatColor = new Color(255 / 255f, 247 / 255f, 89 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, spitCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "spitfire_eyes.png"));
                pony.mane = new PonyPart(spitfireMane, CreateMaterial(TEX_DIR + "spitfire_mane.png"));
                pony.trait = "Pilot";
                pony.courage = 0.932f;
                pony.stupidity = 0.394f;
            }

            {
                pony = CreatePony("Cheerilee");
                var cheeryCoatColor = new Color(188 / 255f, 91 / 255f, 147 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, cheeryCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "cheerilee_eyes.png"));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(TEX_DIR + "cheerilee_mane.png"));
                pony.trait = "Scientist";
                pony.courage = 0.432f;
                pony.stupidity = 0.354f;
            }

            {
                pony = CreatePony("Junebug");
                var juneCoatColor = new Color(239 / 255f, 245 / 255f, 183 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, juneCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + junebugEyes));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(TEX_DIR + "junebug_mane.png"));
                pony.trait = "Scientist";
                pony.courage = 0.1f;
                pony.stupidity = 0.4f;
            }

            {
                pony = CreatePony("Silver Spanner");
                var spannerCoatColor = new Color(181 / 255f, 112 / 255f, 74 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, spannerCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "silver_eyes.png"));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(new Color(252 / 255f, 250 / 255f, 251 / 255f)));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, spannerCoatColor));
                pony.trait = "Engineer";
                pony.courage = 0.729f;
                pony.stupidity = 0.088f;
            }

            {
                pony = CreatePony("Serena");
                var serenaCoatColor = new Color(169 / 255f, 240 / 255f, 251 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, serenaCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "serena_eyes.png"));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(new Color(252 / 255f, 253 / 255f, 255 / 255f)));
                pony.trait = "Pilot";
                pony.courage = 0.942f;
                pony.stupidity = 0.931f;
            }

            {
                pony = CreatePony("Minuette");
                var minuetteCoatColor = new Color(132 / 255f, 200 / 255f, 243 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, minuetteCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "minuette_eyes.png"));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "minuette_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, minuetteCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.887f;
                pony.stupidity = 0.1332f;
            }

            {
                pony = CreatePony("Perfect Pie");
                var perfectCoatColor = new Color(253 / 255f, 197 / 255f, 85 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, perfectCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "perfect_eyes.png"));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(TEX_DIR + "perfect_mane.png"));
                pony.trait = "Engineer";
                pony.courage = 0.928f;
                pony.stupidity = 0.846f;
            }

            {
                pony = CreatePony("Tropical Spring");
                var tropicspringCoatColor = new Color(236 / 255f, 191 / 255f, 149 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, tropicspringCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + pinkieEyes));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(235 / 255f, 104 / 255f, 148 / 255f)));
                pony.trait = "Engineer";
                pony.courage = 0.834f;
                pony.stupidity = 0.659f;
            }

            {
                pony = CreatePony("Sunset Bliss");
                var sunsetblissCoatColor = new Color(228 / 255f, 180 / 255f, 228 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, sunsetblissCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + sunsetEyes));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "sunset_mane.png"));
                pony.trait = "Scientist";
                pony.courage = 0.224f;
                pony.stupidity = 0.325f;
            }

            {
                pony = CreatePony("Deep Blue");
                var deepCoatColor = new Color(203 / 255f, 242 / 255f, 248 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, deepCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "deep_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(160 / 255f, 115 / 255f, 71 / 255f)));
                pony.trait = "Scientist";
                pony.courage = 0.764f;
                pony.stupidity = 0.84f;
            }

            {
                pony = CreatePony("Sun Streak");
                var sunstreakCoatColor = new Color(190 / 255f, 221 / 255f, 231 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, sunstreakCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "sunstreak_eyes.png"));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(TEX_DIR + "sunstreak_mane.png"));
                pony.trait = "Pilot";
                pony.courage = 0.518f;
                pony.stupidity = 0.156f;
            }

            {
                pony = CreatePony("Arctic Lily");
                var arcticCoatColor = new Color(141 / 255f, 202 / 255f, 212 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, arcticCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "arctic_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(152 / 255f, 160 / 255f, 194 / 255f)));
                pony.trait = "Pilot";
                pony.courage = 0.951f;
                pony.stupidity = 0.183f;
            }

            {
                pony = CreatePony("Lemony Gem");
                var lemonyCoatColor = new Color(231 / 255f, 235 / 255f, 152 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, lemonyCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "lemony_eyes.png"));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "lemony_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, lemonyCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.443f;
                pony.stupidity = 0.7654f;
            }

            {
                pony = CreatePony("High Spirits");
                var highsCoatColor = new Color(241 / 255f, 235 / 255f, 159 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, highsCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "high_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(245 / 255f, 190 / 255f, 186 / 255f)));
                pony.trait = "Engineer";
                pony.courage = 0.899f;
                pony.stupidity = 0.745f;
            }

            {
                pony = CreatePony("Blue October");
                var blueCoatColor = new Color(149 / 255f, 222 / 255f, 244 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, blueCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + blueEyes));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(46 / 255f, 122 / 255f, 178 / 255f)));
                pony.trait = "Pilot";
                pony.courage = 0.892f;
                pony.stupidity = 0.311f;
            }

            {
                pony = CreatePony("Amethyst Star");
                var amethyststarCoatColor = new Color(242 / 255f, 141 / 255f, 247 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, amethyststarCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "amethyst_eyes.png"));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "amethyst_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, amethyststarCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.37f;
                pony.stupidity = 0.476f;
            }

            {
                pony = CreatePony("Electric Sky");
                var elesCoatColor = new Color(245 / 255f, 237 / 255f, 133 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, elesCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "minuette_eyes.png"));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "electric_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, elesCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.314f;
                pony.stupidity = 0.35f;
            }

            {
                pony = CreatePony("Blossomforth");
                pony.head = new PonyPart(ponyHead, CreateMaterial(TEX_DIR + "blossomforth_body.png"));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "blossomforth_eyes.png"));
                pony.mane = new PonyPart(twilightMane, CreateMaterial(TEX_DIR + "blossomforth_mane.png"));
                pony.trait = "Pilot";
                pony.courage = 0.26f;
                pony.stupidity = 0.094f;
            }

            {
                pony = CreatePony("Twinkleshine");
                var tinkleCoatColor = new Color(246 / 255f, 246 / 255f, 241 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, tinkleCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "twinkleshine_eyes.png"));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(new Color(255 / 255f, 173 / 255f, 251 / 255f)));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, tinkleCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.13f;
                pony.stupidity = 0.222f;
            }

            {
                pony = CreatePony("Banana Fluff");
                var nanaCoatColor = new Color(214 / 255f, 208 / 255f, 216 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, nanaCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "banana_eyes.png"));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "banana_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, nanaCoatColor));
                pony.trait = "Engineer";
                pony.courage = 0.155f;
                pony.stupidity = 0.865f;
            }

            {
                pony = CreatePony("Perfect Timing");
                var perfecttimingCoatColor = new Color(224 / 255f, 227 / 255f, 172 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, perfecttimingCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + applejackEyes));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(new Color(182 / 255f, 170 / 255f, 90 / 255f)));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, perfecttimingCoatColor));
                pony.trait = "Pilot";
                pony.courage = 0.828f;
                pony.stupidity = 0.567f;
            }

            //I JUST realized that we have "Perfect Timing" AND "Perfect Pie." It miraculously still works out here, but for future reference...
            //Let's use full names from now on. (ex, perfecttiming_eyes.png instead of perfect_eyes.png)

            {
                pony = CreatePony("Passionate");
                var passionateCoatColor = new Color(242 / 255f, 254 / 255f, 175 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, passionateCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + blueEyes));
                pony.mane = new PonyPart(lyraMane, CreateMaterial(TEX_DIR + "passionate_mane.png"));
                pony.horn = new PonyPart(ponyHorn, CreateMaterial(hornTexture, passionateCoatColor));
                pony.trait = "Scientist";
                pony.courage = 0.82f;
                pony.stupidity = 0.77f;
            }

            {
                pony = CreatePony("Jade");
                var jadeCoatColor = new Color(166 / 255f, 206 / 255f, 164 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, jadeCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "jade_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(179 / 255f, 125 / 255f, 191 / 255f)));
                pony.trait = "Scientist";
                pony.courage = 0.442f;
                pony.stupidity = 0.273f;
            }

            {
                pony = CreatePony("Daring Do");
                var daringdoCoatColor = new Color(221 / 255f, 203 / 255f, 104 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, daringdoCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "rainbow_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(TEX_DIR + "daring_mane.png"));
                pony.trait = "Pilot";
                pony.courage = 0.97f;
                pony.stupidity = 0.331f;
            }

            {
                pony = CreatePony("Elbow Grease");
                var elbowgreaseCoatColor = new Color(231 / 255f, 116 / 255f, 205 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, elbowgreaseCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "elbow_eyes.png"));
                pony.mane = new PonyPart(rainbowMane, CreateMaterial(new Color(171 / 255f, 192 / 255f, 240 / 255f)));
                pony.trait = "Engineer";
                pony.courage = 0.1222f;
                pony.stupidity = 0.35f;
            }

            {
                pony = CreatePony("Vanilla Sweets");
                var vanillasweetsCoatColor = new Color(189 / 255f, 189 / 255f, 189 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, vanillasweetsCoatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + trixieEyes));
                pony.mane = new PonyPart(sweetMane, CreateMaterial(new Color(124 / 255f, 160 / 255f, 155 / 255f)));
                pony.trait = "Scientist";
                pony.courage = 0.554f;
                pony.stupidity = 0.55f;
            }

            {
                pony = CreatePony("Berry Punch");
                ponies["berryshine"] = pony;
                var coatColor = new Color(0xDF / 255f, 0x9B / 255f, 0xF2 / 255f);
                var maneColor = new Color(0xC0 / 255f, 0x51 / 255f, 0x96 / 255f);
                pony.head = new PonyPart(ponyHead, CreateMaterial(bodyTexture, coatColor));
                pony.eyes = new PonyPart(eyes, CreateMaterial(TEX_DIR + "rainbow_eyes.png"));
                pony.mane = new PonyPart(berryMane, CreateMaterial(maneColor));
                pony.trait = "Pilot";
                pony.courage = 0.8157f;
                pony.stupidity = 0.7903f;
            }

        }

        private static Matrix4x4 GetBindPose()
        {
            var eva = PartLoader.getPartInfoByName("kerbalEVAfemale").partPrefab.gameObject;
            foreach (SkinnedMeshRenderer smr in eva.GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                switch (smr.name)
                {
                    case "headMesh01":
                    case "mesh_female_kerbalAstronaut01_kerbalGirl_mesh_polySurface51":
                    case "headMesh":
                        int i = 0;
                        foreach(var bone in smr.bones)
                        {
                            if (bone.name == "bn_upperJaw01")
                            {
                                return smr.sharedMesh.bindposes[i];
                            }
                            i++;
                        }
                        break;
                }
            }
            System.Diagnostics.Debug.Assert(false, "GetBindPose failed");
            return new Matrix4x4();
        }

        private Mesh CreateMesh(
            Vector3[] vertices,
            Vector2[] texcoords,
            Vector3[] normals,
            int[] triangles
        )
        {
            var mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.uv = texcoords;
            BoneWeight[] weights = new BoneWeight[mesh.vertices.Length];
            for(int i = 0; i < weights.Length; i++)
            {
                weights[i].boneIndex0 = 0;
                weights[i].weight0 = 1;
            }
            mesh.boneWeights = weights;
            mesh.bindposes = new Matrix4x4[] {bindpose};
            mesh.triangles = triangles;
            if (normals is null) {
                mesh.RecalculateNormals();
            } else {
                mesh.normals = normals;
            }

            return mesh;
        }

        private Vector3[] LoadVector3Array(string path)
        {
            using(FileStream fs = File.OpenRead(path))
            using(BinaryReader r = new BinaryReader(fs))
            {
                var a = new Vector3[r.ReadUInt32()];
                for(int i = 0; i < a.Length; i++)
                {
                    a[i].x = r.ReadSingle();
                    a[i].y = r.ReadSingle();
                    a[i].z = r.ReadSingle();
                }
                return a;
            }
        }

        private Vector3[] LoadVector3ArrayIfExists(string path) {
            if (File.Exists(path)) {
                return LoadVector3Array(path);
            } else {
                return null;
            }
        }


        private Vector2[] LoadVector2Array(string path)
        {
            using(FileStream fs = File.OpenRead(path))
            using(BinaryReader r = new BinaryReader(fs))
            {
                var a = new Vector2[r.ReadUInt32()];
                for(int i = 0; i < a.Length; i++)
                {
                    a[i].x = r.ReadSingle();
                    a[i].y = r.ReadSingle();
                }
                return a;
            }
        }

        private int[] LoadIntArray(string path)
        {
            using(FileStream fs = File.OpenRead(path))
            using(BinaryReader r = new BinaryReader(fs))
            {
                var a = new int[r.ReadUInt32()];
                for(int i = 0; i < a.Length; i++)
                {
                    a[i] = r.ReadInt32();
                }
                return a;
            }
        }

        private Mesh LoadMesh(string name)
        {
            string basepath = MODELS_DIR + name;
            return CreateMesh(
                LoadVector3Array(basepath + ".vtx"),
                LoadVector2Array(basepath + ".tex"),
                LoadVector3ArrayIfExists(basepath + ".nml"),
                LoadIntArray(basepath + ".idx")
            );
        }

        private Material CreateMaterial(Color color)
        {
            var mat = new Material(Shader.Find("Diffuse"));
            mat.color = color;
            return mat;
        }

        private Texture2D LoadTexture(string path)
        {
            path = path.Substring(0, path.LastIndexOf('.'));
            Texture2D tex = null;
            textures.TryGetValue(path, out tex);
            return tex;
        }

        private void FillTexturesDict() 
        {
            foreach(var tex in Resources.FindObjectsOfTypeAll<Texture2D>())
            {
                textures[tex.name] = tex;
            }
        }

        private Material CreateMaterial(string path)
        {
            var tex = LoadTexture(path);
            return CreateMaterial(tex);
        }

        private Material CreateMaterial(Texture2D texture)
        {
            return CreateMaterial(texture, Color.white);
        }

        private Material CreateMaterial(Texture2D texture, Color color)
        {
            var mat = new Material(Shader.Find("Diffuse"));
            mat.mainTexture = texture;
            mat.color = color;
            return mat;
        }

        public Pony GetPony(string name)
        {
            Pony pony = null;
            ponies.TryGetValue(name, out pony);
            return pony;
        }
    }
}

