#!/usr/bin/python3
import json
from typing import *
from pathlib import Path
from data_types import Vector, get_bounds, Mesh
from smd_parser import SMDParser
from obj_parser import load_obj_model
from mod_models import load_mod_model


def init_models_js(models):
    code = 'var mesh, geom, material;\n'
    for m in models:
        color = getattr(m, 'color', '0xEDBBF3')
        code += 'material = new THREE.MeshLambertMaterial( { color: %s } );\n' % color
        code += 'geom = new THREE.BufferGeometry();\n'

        vertices = 'new Float32Array(%s)' % json.dumps([x for v in m.vertices for x in v])
        code += 'geom.addAttribute( \'position\', new THREE.BufferAttribute( %s, 3 ) );\n' % vertices

        if not getattr(m, 'normals', None):
            code += 'geom.computeVertexNormals();\n'
            code += 'geom.normalizeNormals();\n'
        else:
            normals = 'new Float32Array(%s)' % json.dumps([x for v in m.normals for x in v])
            code += 'geom.addAttribute(\'normal\', new THREE.BufferAttribute( %s, 3 ) );\n' % normals

        indices = 'new Uint32Array(%s)' % json.dumps(m.indices)
        code += 'geom.index = new THREE.BufferAttribute(%s, 1);\n' % indices
        code += 'scene.add( new THREE.Mesh( geom, material ));\n'

    return 'function initModels(scene) { \n%s }' % code


def clip_pony_head(
    model: Mesh,
    x1 = 0.482320285, y1 = -0.19379485,
    x2 = 1.3385514316666671, y2 = 0.39486406333333335,
):
    a = y2 - y1
    b = x1 - x2
    c = a*x1+b*y1

    v = [
        (i, p) for i, p in enumerate(model.vertices)
        if a*p[2]-b*p[1]-c > 0
    ]
    vertices = [p for _, p in v]
    normals = None
    if model.normals:
        normals = [
            model.normals[i]
            for i, _ in v
        ]
    texcoords = [
        model.texcoords[i]
        for i, _ in v
    ]
    index_map = {i: j for j, (i, _) in enumerate(v)}
    assert all(vertices[j] for j in index_map.values())

    indices = [
        index_map[i]
        for face in model.iter_triangles()
        if all(i in index_map for i in face)
        for i in face
    ]

    v1 = [
        model.vertices[i]
        for face in model.iter_triangles()
        if all(i in index_map for i in face)
        for i in face
    ]
    v2 = [vertices[i] for i in indices]
    assert (v1 == v2)

    result = model.clone()
    result.vertices = vertices
    result.texcoords = texcoords
    result.indices = indices
    if normals:
        result.normals = normals
    return result


def get_transform_for_obj_model_v1(model: Mesh, valentinas_head: Mesh):
    pony_head = model.vertices
    bounds = get_bounds(pony_head)
    vbounds = get_bounds(valentinas_head.vertices)

    f = max((vbounds[1] - vbounds[0]).vdiv(bounds[1] - bounds[0]))
    dv = ((vbounds[1] + vbounds[0]) / f - (bounds[1] + bounds[0])) / 2
    dv = dv + Vector([0, -0.047, 0.05]) / f
    return lambda d: dict(d, vertices=[(Vector(v) + dv) * f for v in d['vertices']])


def get_top_point(mesh: Mesh):
    return max(mesh.vertices, key=lambda v: v[1])


def get_nose_point(mesh: Mesh):
    return max(mesh.vertices, key=lambda v: v[2])


def get_transform_for_obj_model(model: Mesh, ref_model: Mesh):
    # ref_model - reference model with correct sizing and offset from mod distribution
    top = get_top_point(model)
    nose = get_nose_point(model)
    ref_top = get_top_point(ref_model)
    ref_nose = get_nose_point(ref_model)

    scale_factor = abs(ref_top[2] - ref_nose[2]) / abs(top[2] - nose[2])
    offset = (Vector([0, 0, 0]) - top) * scale_factor + ref_top
    offset = list(offset)
    offset[0] = 0
    offset = Vector(offset)

    def transform(mesh: Mesh) -> Mesh:
        return mesh.transform(
            scale=scale_factor,
            offset=offset,
        )

    return transform


def load_smd_body():
    smd_parser = SMDParser()
    with open('decompiled/vn_twilightsparkle/vn_twilightsparkle_reference_twilight.smd') as f:
        smd_parser.parse_smd(f)

    with open('decompiled/vn_twilightsparkle/vn_twilightsparkle_01.vta') as f:
        smd_parser.parse_vta(f)

    smd_model = (
        smd_parser.meshes['body']
            .remap_coordinates(0, 2, 1)
            .scale3(Vector([1, 1, -1]))
    )

    return smd_model


def load_smd_model(p) -> Dict[str, Mesh]:
    p = Path(p)
    smd_parser = SMDParser()
    with p.open() as f:
        smd_parser.parse_smd(f)
    return {
        name: mesh.remap_coordinates(0, 2, 1)
                  .scale3(Vector([1, 1, -1]))
        for name, mesh in smd_parser.meshes.items()
    }


def load_mod_model_ex(base_path):
    return load_mod_model(
        idx_path=f'{base_path}.idx',
        vtx_path=f'{base_path}.vtx',
    )


if __name__ == '__main__':
    smd_model = load_smd_body()
    head = load_mod_model_ex('models/Head')

    manes = {
        f'{p.parent.name[3:]}/{mesh_name}': mesh
        for x in '''
            vn_berrypunch/vn_berrypunch_mane_berrypunch.smd
            vn_cloudchaser/vn_cloudchaser_mane_cloudchaser.smd
            vn_octavia/vn_octavia_mane_octavia.smd
            vn_rainbowdash/vn_rainbowdash_mane_RD.smd
            vn_spitfire/vn_spitfire_mane_spitfire.smd
            vn_starlight_glimmer/vn_starlight_glimmer_s6_mane_Starlight.smd
            vn_twilightsparkle/vn_twilightsparkle_mane_twilight.smd
            vn_vinyl_scratch/vn_vinyl_scratch_mane_scratch.smd
        '''.split()
        for p in [Path('decompiled', x)]
        for mesh_name, mesh in load_smd_model(p).items()
    }

    hair_colors = {
        'berrypunch/hair': ('0xFCF260', '0xFF80BF'),
        'starlight_glimmer/Mane': ('0x622489', '0xEDBBF3'),
        'spitfire/hair': ('0xED7800', '0xF6E663'),
    }

    print("Manes:")
    for k in manes.keys():
        print(k)
    print()

    with open('models/obj/high-res_pony_v1.1_body.OBJ') as f:
    # with open('models/obj/pony_body.OBJ') as f:
        obj_body = load_obj_model(f)
    # Normals in obj file seem broken
    obj_body.normals = []

    smd_to_obj = get_transform_for_obj_model(smd_model, obj_body)
    smd_pony_body = smd_to_obj(smd_model)

    smd_pony_head = clip_pony_head(smd_pony_body)
    obj_pony_head = clip_pony_head(obj_body)

    obj_to_ksp = get_transform_for_obj_model(obj_pony_head, head)
    obj_pony_head = obj_to_ksp(obj_pony_head)
    smd_pony_head = obj_to_ksp(smd_pony_head)

    mane_name = 'spitfire/hair'
    mane = obj_to_ksp(smd_to_obj(manes[mane_name]))
    mane.color, smd_pony_head.color = hair_colors.get(mane_name, ('0x622489', '0xEDBBF3'))
        
    # print(parser.meshes.keys())
    # for k, m in parser.meshes.items():
    #     print(k + ':')
    #     for aname in m.morph_targets.keys():
    #         print('   ', aname)
    # print(json.dumps(parser.vta_anims, indent=4))

    if 0:
        Path('viewer', 'data.js').write_text(init_models_js([
            # head,
            # obj_body,
            # pony_head,
            # smd_model,
            smd_pony_head,
            mane
        ]))

    mod_data_path = r'F:\Program Files (x86)\Steam\steamapps\common\Kerbal Space Program\GameData\KerbalSpacePonies'
    smd_pony_head.save_for_ksp(rf'{mod_data_path}\Models\Head')

    mane_outputs = {
        'berrypunch/hair': 'BerryMane',
        'starlight_glimmer/Mane': 'StarlightMane',
        'spitfire/hair': 'SpitfireMane',
        'octavia/hair': 'OctaviaMane',
        'vinyl_scratch/hair': 'VinylMane',
    }

    for k, v in mane_outputs.items():
        print(f"Saving {v}")
        obj_to_ksp(smd_to_obj(manes[k])).save_for_ksp(Path(mod_data_path, 'Models', v))
