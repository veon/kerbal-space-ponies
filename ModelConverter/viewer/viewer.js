var scene, camera, controls, renderer;
var enabledGeoms = {
    body: 1,
    Mane: 1,
    eyeball_l: 1,
    eyeball_r: 1,
};

function makeTextureMaterial(path, { repeat = 1, flipY = true } = {}) {
    var texture = new THREE.TextureLoader().load(path);
    
    if (repeat != 1) {
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(repeat, repeat);
    }
    
    texture.flipY = flipY;
    return new THREE.MeshLambertMaterial({
        map: texture,
        color: 0xffffff,
    });
}

function init() {
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

    /*var geoms = parseSMDMesh(starlight_smd);
    var materials = {
        body: new THREE.MeshLambertMaterial( { color: 0xEDBBF3 } ),
        Mane: makeTextureMaterial('textures/mane.png'),
        eyeball_l: makeTextureMaterial('textures/pupil_l.png', {repeat: 2, flipY: false}),
        eyeball_r: makeTextureMaterial('textures/pupil_r.png', {repeat: 2, flipY: false}),
    };
    for(var name in geoms) {
        if (!enabledGeoms[name]) continue;
        console.log(name);
        var material = materials[name] || materials.body;
        var mesh = new THREE.Mesh( geoms[name], material );
        scene.add( mesh );
    }*/
    //material.wireframe = true;

    initModels(scene);
    
    var light = new THREE.AmbientLight( 0x404040 ); // soft white light
    scene.add( light );
    
    var directionalLight = new THREE.DirectionalLight( 0xffffff );
    directionalLight.position.set(10, 10, 5);
    // directionalLight.target = mesh;
    scene.add( directionalLight );
    
    /*var hlight = new THREE.HemisphereLight( 0xffffbb, 0x080820, 1 );
    scene.add( hlight );*/
    
    /*var light = new THREE.PointLight( 0xff0000, 0, 100 );
    light.position.set( 50, 50, 50 );
    scene.add( light );*/
    
    var gridHelper = new THREE.GridHelper( 10, 20 );
    scene.add( gridHelper )

    renderer = new THREE.WebGLRenderer({
        antialias: true,
    });
    renderer.setSize( window.innerWidth, window.innerHeight );
    let vs = viewSize();
    //renderer.setSize( vs.w, vs.h );
    renderer.setClearColor(0xcccccc);
    document.body.appendChild( renderer.domElement );

    controls = new THREE.OrbitControls( camera );
    controls.target = new THREE.Vector3( 0, 0.5, 0 );

    //controls.update() must be called after any manual changes to the camera's transform
    camera.position.set( 1, 1, 1 );
    controls.update();
    
    let x = {
        x: 1,
    };
    // let gui = new dat.GUI();
    // gui.add(x   , 'x', 0, 200);
}

function animate() {

	requestAnimationFrame( animate );
    renderer.setSize( window.innerWidth, window.innerHeight );

	// required if controls.enableDamping or controls.autoRotate are set to true
	controls.update();

	renderer.render( scene, camera );

}

function docElem( property )
{
  var t
  return ((t = document.documentElement) || (t = document.body.parentNode)) && isNumber( t[property] ) ? t : document.body
}

// View width and height excluding any visible scrollbars
// http://www.highdots.com/forums/javascript/faq-topic-how-do-i-296669.html
//    1) document.client[Width|Height] always reliable when available, including Safari2
//    2) document.documentElement.client[Width|Height] reliable in standards mode DOCTYPE, except for Safari2, Opera<9.5
//    3) document.body.client[Width|Height] is gives correct result when #2 does not, except for Safari2
//    4) When document.documentElement.client[Width|Height] is unreliable, it will be size of <html> element either greater or less than desired view size
//       https://bugzilla.mozilla.org/show_bug.cgi?id=156388#c7
//    5) When document.body.client[Width|Height] is unreliable, it will be size of <body> element less than desired view size
function viewSize()
{
  // This algorithm avoids creating test page to determine if document.documentElement.client[Width|Height] is greater then view size,
  // will succeed where such test page wouldn't detect dynamic unreliability,
  // and will only fail in the case the right or bottom edge is within the width of a scrollbar from edge of the viewport that has visible scrollbar(s).
  var doc = docElem( 'clientWidth' ),
     body = document.body,
     w, h
  return isNumber( document.clientWidth ) ? { w : document.clientWidth, h : document.clientHeight } :
     doc === body
     || (w = Math.max( doc.clientWidth, body.clientWidth )) > self.innerWidth
     || (h = Math.max( doc.clientHeight, body.clientHeight )) > self.innerHeight ? { w : body.clientWidth, h : body.clientHeight } :
     { w : w, h : h }
}

function isNumber(x) {
    return typeof x == 'number';
}

init();
animate();