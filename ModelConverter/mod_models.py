import struct
from typing import *
from data_types import *


def load_array(path, fmt):
    with open(path, 'rb') as f:
        n, = struct.unpack('<L', f.read(4))
        buf = f.read(n * struct.calcsize(fmt))
        return list(struct.iter_unpack(fmt, buf))


def load_mod_model(idx_path, vtx_path, tex_path=None):
    class Mesh:
        vertices: List[Vector3] = load_array(vtx_path, '<fff')
        indices: List[int] = [i for t in load_array(idx_path, '<iii') for i in t]
        texcoords: List[Vector2]

    mesh = Mesh()
    if tex_path:
        mesh.texcoords = load_array(tex_path, '<ff')

    return mesh
