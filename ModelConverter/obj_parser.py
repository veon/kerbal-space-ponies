from data_types import *


def load_obj_model(f):
    vertices = []
    texcoords = []
    normals = []
    faces = []

    for s in f:
        t = s.split()
        if len(t) < 1:
            pass
        elif t[0] == 'v':
            vertices.append(Vector3([float(x) for x in t[1:]]))
        elif t[0] == 'vt':
            texcoords.append(Vector2([float(x) for x in t[1:]]))
        elif t[0] == 'vn':
            normals.append(Vector3([float(x) for x in t[1:]]))
        elif t[0] == 'f':
            faces.append(tuple(
                tuple(int(y)-1 for y in x.split('/'))
                for x in t[1:]
            ))

    m = Mesh('obj')

    index_map = {}
    for face in faces:
        if 0:
            i1, i2, i3, i4 = [
                m.add_vertex(
                    vertices[vi],
                    texcoords[ti],
                    normals[ni],
                )
                for vi, ti, ni in face
            ]
        else:
            inds = []
            for ind in face:
                if len(ind) > 2:
                    vi, ti, ni = ind
                    normal = normals[ni]
                else:
                    vi, ti = ind
                    normal = Vector3([1, 1, 1])
                ind = ind[:2]
                if ind not in index_map:
                    i = m.add_vertex(
                        vertices[vi],
                        texcoords[ti],
                        normal,
                    )
                    index_map[ind] = i
                else:
                    i = index_map[ind]
                inds.append(i)
            i1, i2, i3, i4 = inds

        m.add_triangle(i1, i2, i3)
        m.add_triangle(i1, i3, i4)

    return m