from typing import *
from data_types import *
        

class SMDParser:
    def __init__(self):
        self.current_mesh: Mesh = None
        self.meshes: Dict[MeshName, Mesh] = {}
        self.vert_indices = {}
        self.vta_key_to_smd_indices: Dict[tuple, List[Tuple[MeshName, int]]] = {}
        self.vta_anims_by_id: Dict[int, str] = {}

    def parse_smd(self, f):
        it = iter(f)
        for line in it:
            if 'triangles' == line.strip():
                break
                
        for line in it:
            line = line.rstrip()
            if line.startswith(' '):
                self._parse_vertex(line)
            elif line == 'end':
                return
            else:
                self._parse_mat_name(line)
                
    def _parse_mat_name(self, line):
        name = MeshName(line.strip())
        if name not in self.meshes:
            self.meshes[name] = Mesh(name)
        self.current_mesh = self.meshes[name]
            
    def _parse_vertex(self, line):
        bone_index, vx, vy, vz, nx, ny, nz, u, v, *_ = line.split()
        vert_key = line.strip()
        vta_key = (vx, vy, vz, nx, ny, nz)
        
        i = self.vert_indices.get(vert_key)
        if i is None:
            self.vert_indices[vert_key] = i = self.current_mesh.add_vertex(
                make_vector3(vx, vy, vz),
                make_vector3(nx, ny, nz),
                make_vector2(u, v),
            )
            l = self.vta_key_to_smd_indices.setdefault(vta_key, [])
            l.append((self.current_mesh.name, i))
        else:
            assert self.current_mesh.texcoords[i] == make_vector2(u, v)
        self.current_mesh.indices.append(i)
            
    def parse_vta(self, f):
        it = iter(f)
        for line in it:
            if 'skeleton' == line.strip():
                break
                
        for line in it:
            line = line.rstrip()
            if not line.startswith(' '):
                break
                
            line , _, anim_name = line.partition('#')
            anim_name = anim_name.strip()
            t, i = line.split()
            assert t == 'time'
            
            self.vta_anims_by_id[int(i)] = anim_name
            
        for line in it:
            if 'vertexanimation' == line.strip():
                break

        curr_anim_idx = None
        vta_keys = {}
        for line in it:
            line = line.split()
            
            if line[0] == 'time':
                curr_anim_idx = int(line[1])
                curr_anim_name = self.vta_anims_by_id[curr_anim_idx]
            elif line[0].isnumeric():
                vta_idx, vx, vy, vz, nx, ny, nz = line
                if curr_anim_idx == 0:
                    vta_keys[int(vta_idx)] = (vx, vy, vz, nx, ny, nz)
                else:
                    vta_key = vta_keys[int(vta_idx)]
                    for mesh_name, i in self.vta_key_to_smd_indices[vta_key]:
                        m: Mesh = self.meshes[mesh_name].get_morph_target(curr_anim_name)
                        m.vertices[i] = make_vector3(vx, vy, vz)
                        m.normals[i]  = make_vector3(nx, ny, nz)

            elif line[0] == 'end':
                break
            else:
                raise Exception(line)
        

def make_vector3(*a) -> Vector3:
    assert len(a) == 3
    return Vector3((map(float, a)))
    
    
def make_vector2(*a) -> Vector2:
    assert len(a) == 2
    return Vector2(map(float, a))
